# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 14:26:56 2024

@author: galex
"""




from tkinter import *
from random import *
import time
from tkinter.font import Font
import random
import time

# Variables globales
nom_joueur = [0, 1, 2, 3]
pos_actuel = [0, 0, 0, 0]
possede = [[], [], [], []]
argent = [20000, 20000, 20000, 20000]
plateau_monopoly = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18","19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38"]
prix_case = [0,6000,"non",6000,"non","non",10000,"non",10000,12000,"non",14000,"non",14000,16000,"non",18000,"non",18000,20000,"non",22000,"non",22000,24000,"non",26000,26000,"non",28000,"non",30000,30000,"non",32000,"non","non",35000,"non",40000]
case_train = ["0", "1", "2", "3", "4", "train_bas", "6", "7", "8", "9", "10", "11", "12", "13", "14", "train_gauche", "16", "17", "18","19", "20", "21", "22", "23", "24", "train_haut", "26", "27", "28", "29", "30", "31", "32", "33", "train_droite", "35", "36", "37", "38"]
nom_cases = []
joueurs = []
tour_joueur = 0
nb_joueurs = 4  



coordonnees_noir = [
    (671, 575, 681, 585), (629, 575, 639, 585), (592, 575, 602, 585),
    (558, 575, 568, 585), (522, 575, 532, 585), (486, 575, 496, 585),
    (450, 575, 460, 585), (414, 575, 424, 585), (378, 575, 388, 585),
    (342, 575, 352, 585), (291, 575, 301, 585),
    (291, 529, 301, 539), (291, 493, 301, 503), (291, 457, 301, 467),
    (291, 421, 301, 431), (291, 385, 301, 395), (291, 349, 301, 359),
    (291, 313, 301, 323), (291, 277, 301, 287), (291, 241, 301, 251),
    (291, 193, 301, 203),
    (342, 193, 352, 203), (378, 193, 388, 203), (414, 193, 424, 203),
    (450, 193, 460, 203), (486, 193, 496, 203), (522, 193, 532, 203),
    (558, 193, 568, 203), (592, 193, 602, 203), (629, 193, 639, 203),
    (671, 193, 681, 203),
    (671, 241, 681, 251), (671, 277, 681, 287), (671, 313, 681, 323),
    (671, 349, 681, 359), (671, 395, 681, 395), (671, 421, 681, 431),
    (671, 457, 681, 467), (671, 493, 681, 503), (671, 529, 681, 539)]


coordonnees_bleu = [
    (691, 575, 701, 585), (649, 575, 659, 585), (612, 575, 622, 585),
    (578, 575, 588, 585), (542, 575, 552, 585), (506, 575, 516, 585),
    (470, 575, 480, 585), (434, 575, 444, 585), (398, 575, 408, 585),
    (362, 575, 372, 585), (311, 575, 321, 585),
    (311, 529, 321, 539), (311, 493, 321, 503), (311, 457, 321, 467),
    (311, 421, 321, 431), (311, 385, 321, 395), (311, 349, 321, 359),
    (311, 313, 321, 323), (311, 277, 321, 287), (311, 241, 321, 251),
    (311, 193, 321, 203),
    (362, 193, 372, 203), (398, 193, 408, 203), (434, 193, 444, 203),
    (470, 193, 480, 203), (506, 193, 516, 203), (542, 193, 552, 203),
    (578, 193, 588, 203), (612, 193, 622, 203), (649, 193, 659, 203),
    (691, 193, 701, 203),
    (691, 241, 701, 251), (691, 277, 701, 287), (691, 313, 701, 323),
    (691, 349, 701, 359), (691, 395, 701, 395), (691, 421, 701, 431),
    (691, 457, 701, 467), (691, 493, 701, 503), (691, 529, 701, 539)]


coordonnees_jaune = [
    (691, 595, 701, 605), (649, 595, 659, 605), (612, 595, 622, 605),
    (578, 595, 588, 605), (542, 595, 552, 605), (506, 595, 516, 605),
    (470, 595, 480, 605), (434, 595, 444, 605), (398, 595, 408, 605),
    (362, 595, 372, 605), (311, 595, 321, 605),
    (311, 549, 321, 559), (311, 513, 321, 523), (311, 477, 321, 487),
    (311, 441, 321, 451), (311, 405, 321, 415), (311, 369, 321, 379),
    (311, 333, 321, 343), (311, 297, 321, 307), (311, 261, 321, 271),
    (311, 213, 321, 223),
    (362, 213, 372, 223), (398, 213, 408, 223), (434, 213, 444, 223),
    (470, 213, 480, 223), (506, 213, 516, 223), (542, 213, 552, 223),
    (578, 213, 588, 223), (612, 213, 622, 223), (649, 213, 659, 223),
    (691, 213, 701, 223),
    (691, 261, 701, 271), (691, 297, 701, 307), (691, 333, 701, 343),
    (691, 369, 701, 379), (691, 415, 701, 425), (691, 441, 701, 451),
    (691, 477, 701, 487), (691, 513, 701, 523), (691, 549, 701, 559)]


coordonnees_rouge = [
    (671, 595, 681, 605), (629, 595, 639, 605), (592, 595, 602, 605),
    (558, 595, 568, 605), (522, 595, 532, 605), (486, 595, 496, 605),
    (450, 595, 460, 605), (414, 595, 424, 605), (378, 595, 388, 605),
    (342, 595, 352, 605), (291, 595, 301, 605),
    (291, 549, 301, 559), (291, 513, 301, 523), (291, 477, 301, 487),
    (291, 441, 301, 451), (291, 405, 301, 415), (291, 369, 301, 379),
    (291, 333, 301, 343), (291, 297, 301, 307), (291, 261, 301, 271),
    (291, 213, 301, 223),
    (342, 213, 352, 223), (378, 213, 388, 223), (414, 213, 424, 223),
    (450, 213, 460, 223), (486, 213, 496, 223), (522, 213, 532, 223),
    (558, 213, 568, 223), (592, 213, 602, 223), (629, 213, 639, 223),
    (671, 213, 681, 223),
    (671, 261, 681, 271), (671, 297, 681, 307), (671, 333, 681, 343),
    (671, 369, 681, 379), (671, 415, 681, 425), (671, 441, 681, 451),
    (671, 477, 681, 487), (671, 513, 681, 523), (671, 549, 681, 559)]

coordonnee=[coordonnees_noir, coordonnees_bleu, coordonnees_jaune, coordonnees_rouge]
# Fonctions

def deplacer_pion_graphique():
    ## deplace  le pion du joueur actif sur sa position 
    # la variable joueurs c'est la liste des quatre oval
    # la variable tour_joueur c'est le num du joeur (0,1,2,3)
    # la variable pos_actuel est une liste avec les num des cases de chaque joueur
    # la liste coordonnee contient les coordonnées des oval : ex coordonnée[0][2]=(592, 575, 602, 585)
    # cad : le joueur 0 (noir) s'il est sur la case 2 on doit changer ses coordonnée en (592, 575, 602, 585)
    case=pos_actuel[tour_joueur]  # la case du joueur
    coord=coordonnee[tour_joueur][case] # ses coordonnées
    print (tour_joueur,pos_actuel,case,coord )
    Canevas.coords(joueurs[tour_joueur],coord[0],coord[1],coord[2],coord[3]) # on change les coordonnées
    


def affiche_texte_console(texte_a_ecrire):
    Lab.configure(text=texte_a_ecrire)
    
def affiche_texte_console2(texte_a_ecrire):
    Lab2.configure(text=texte_a_ecrire)

def lancer_de():
   return random.randint(1,6)
    
def nbparc():
    return random.randint(10000,40000)
    
def lancer_de_carte_chance():
    return random.randint(1, 5)

def deplacer_pion(pos_actuelle, dé):
    if pos_actuelle + dé < len(plateau_monopoly):
        nouvelle_pos = pos_actuelle + dé
        return plateau_monopoly[nouvelle_pos]
    else:
        nouvelle_pos = (pos_actuelle + dé) % len(plateau_monopoly)
        return plateau_monopoly[nouvelle_pos]

def case_chance():
    global pos_actuel, argent ,tour_joueur 
    numero_chance = lancer_de_carte_chance()
    joueur=tour_joueur
    if numero_chance == 1:
        pos_actuel[joueur] = 0
        print("Avancez jusqu'à la case Départ : Le joueur avance immédiatement son pion jusqu'à la case Départ et reçoit le salaire correspondant.")
        affiche_texte_console("Avancez jusqu'à la case Départ")
        affiche_texte_console2("Le joueur avance immédiatement son pion jusqu'à la case Départ et reçoit le salaire correspondant.")
    elif numero_chance == 2:
        pos_actuel[joueur] -= 3
        print("Reculez de trois cases : Le joueur recule son pion de trois cases.")
        affiche_texte_console("Reculez de trois cases ")
        affiche_texte_console2("Le joueur recule son pion de trois cases.")
    elif numero_chance == 3:
        argent[joueur] += 1500
        print("joueur de rue : Le joueur recoit un pris pour avoir gagner a un jeux de chance (+1500)")
        affiche_texte_console(" joueur de rue ")
        affiche_texte_console2("Le joueur recoit un pris pour avoir gagner a un jeux de chance (+1500)")
    elif numero_chance == 4:
        pos_actuel[joueur]=12
        argent[joueur] -= 15000
        print("Avancez jusqu'à la case compagnie d’éléctricité : Le joueur se déplace jusqu'à la case Compagnie d'électricité et doit payer le loyer .")
        affiche_texte_console("Avancez jusqu'à la case compagnie d’éléctricité : ")
        affiche_texte_console2(" Le joueur se déplace jusqu'à la case Compagnie d'électricité et doit payer le loyer . ")
    elif numero_chance ==5:
        argent[joueur] += 15000
        print("tours gratuite :le joueur fait un tours complét et reviens a la case d’où il est partie, il reçoit egalement sa paye.")
        affiche_texte_console("tours gratuite")
        affiche_texte_console2("le joueur fait un tours complét et reviens a la case d’où il est partie, il reçoit egalement sa paye.")
    time.sleep(5)
    tour_joueur = (tour_joueur + 1) % nb_joueurs


def Dé():
    global pos_actuel, tour_joueur 
    x=155
    y=75
    affiche_argent()
    chiffre = lancer_de()
    print (tour_joueur,pos_actuel,chiffre)
    pos_actuel[tour_joueur]=(chiffre + pos_actuel[tour_joueur])%39
    
    print (tour_joueur,pos_actuel,chiffre)
    image=Canevas.create_rectangle(x-50,y-50,x+50,y+50,fill='white')
    if chiffre==1:
        image=Canevas.create_oval(x-15,y-15,x+15,y+15,fill='black')#de millieux
        print("1")
    elif chiffre==2:
        image=Canevas.create_oval(x-45,y-45,x-15,y-15,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y+15,x+45,y+45,fill='black')#de bas droit
        print("2")
    elif chiffre==3:
        image=Canevas.create_oval(x-45,y-45,x-15,y-15,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y+15,x+45,y+45,fill='black')#de bas droit
        image=Canevas.create_oval(x-15,y-15,x+15,y+15,fill='black')#de millieux
        print("3")
    elif chiffre==4:
        image=Canevas.create_oval(x-45,y-45,x-15,y-15,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y+15,x+45,y+45,fill='black')#de bas droit
        image=Canevas.create_oval(x-45,y+15,x-15,y+45,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y-45,x+45,y-15,fill='black')#de bas droit
        print("4")
    elif chiffre==5:
        image=Canevas.create_oval(x-45,y-45,x-15,y-15,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y+15,x+45,y+45,fill='black')#de bas droit
        image=Canevas.create_oval(x-45,y+15,x-15,y+45,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y-45,x+45,y-15,fill='black')#de bas droit
        image=Canevas.create_oval(x-15,y-15,x+15,y+15,fill='black')#de millieux
        print("5")
    elif chiffre==6:
        image=Canevas.create_oval(x-45,y-45,x-15,y-15,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y+15,x+45,y+45,fill='black')#de bas droit
        image=Canevas.create_oval(x-45,y+15,x-15,y+45,fill='black')#de haut gauche
        image=Canevas.create_oval(x+15,y-45,x+45,y-15,fill='black')#de bas droit
        image=Canevas.create_oval(x-45,y-15,x-15,y+15,fill='black')#de millieux
        image=Canevas.create_oval(x+15,y-15,x+45,y+15,fill='black')#de millieux
        print("6")
    bouton_de.place(x=-1000, y=-1000)
    fonction_reconnaissance_case()
    
def Joueur_suivant():
    global tour_joueur
    '''
    raméne le bouton lancer dés
    change la variable tour_joueur
    affiche les pion aux bonnes positions
    affiche l'argent correct
    affiche message joueur suivant
    '''
    bouton_de.place(x=25, y=50)
    deplacer_pion_graphique()
    tour_joueur=(tour_joueur+1)%4
    affiche_argent()
    n=tour_joueur + 1
    affiche_texte_console("C est au tour du joueur "+str(n)+" de Lancer le dé.")
    affiche_texte_console2("")
    bouton_suivant.place(x=-783, y=-161) 
    

def bouton_commencer():
    global tour_joueur
    tour_joueur = 0
    bouton_reco.place(x=24, y=24)
    bouton_commencer.place(x=-1000, y=-1000)
    bouton_de.place(x=25, y=50)
    affiche_texte_console(f"C'est au tour du joueur {tour_joueur + 1}. Lancer le dé.")
    



def bouton_suivant():
    global tour_joueur
    tour_joueur = (tour_joueur + 1) % nb_joueurs
    affiche_texte_console(f"C'est au tour du joueur {tour_joueur + 1}. Lancer le dé.")
    print(f"C'est au tour du joueur {tour_joueur + 1}.")
 
  
    
    

def recommencer():
    nom_joueur = [0, 1, 2, 3]
    pos_actuel = [0, 0, 0, 0]
    possede = [[], [], [], []]
    argent = [20000, 20000, 20000, 20000]
    plateau_monopoly = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18","19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38"]
    prix_case = [0,6000,"non",6000,"non","non",10000,"non",10000,12000,"non",14000,"non",14000,16000,"non",18000,"non",18000,20000,"non",22000,"non",22000,24000,"non",26000,26000,"non",28000,"non",30000,30000,"non",32000,"non","non",35000,"non",40000]
    case_train = ["0", "1", "2", "3", "4", "train_bas", "6", "7", "8", "9", "10", "11", "12", "13", "14", "train_gauche", "16", "17", "18","19", "20", "21", "22", "23", "24", "train_haut", "26", "27", "28", "29", "30", "31", "32", "33", "train_droite", "35", "36", "37", "38"]
    nom_cases = []
    joueurs = []
    tour_joueur = 0
    nb_joueurs = 4
    bouton_commencer.place(x=24, y=24)
    bouton_reco.place(x=-1000,y=-1000)
    affiche_texte_console("vous avez recommencer la partie a zero, veuiller clicker sur commencer")
    print("bouton recomencer cliker")

    
def affiche_argent():
    Lab_argent_joueur1.configure(text=argent[0])
    Lab_argent_joueur2.configure(text=argent[1])
    Lab_argent_joueur3.configure(text=argent[2])
    Lab_argent_joueur4.configure(text=argent[3])
    
    
def acheter_case():
    global tour_joueur
    global possede
    global argent 
    if pos_actuel[tour_joueur] in possede[tour_joueur]:
        affiche_texte_console2("vous posséder deja cette case")
        bouton_suivant.place(x=783, y=161) 
    elif pos_actuel[tour_joueur] in possede[0]:
        affiche_texte_console2("vous devez payer le prix de la case a son propriéter")
        argent[tour_joueur]-=prix_case[pos_actuel[tour_joueur]]
        argent[0]+=prix_case[pos_actuel[tour_joueur]]
        bouton_suivant.place(x=783, y=161) 
    elif pos_actuel[tour_joueur] in possede[1]:
        affiche_texte_console2("vous devez payer le prix de la case a son propriéter")
        argent[tour_joueur]-=prix_case[pos_actuel[tour_joueur]]
        argent[1]+=prix_case[pos_actuel[tour_joueur]]
        bouton_suivant.place(x=783, y=161) 
    elif pos_actuel[tour_joueur] in possede[2]:
        affiche_texte_console2("vous devez payer le prix de la case a son propriéter")
        argent[tour_joueur]-=prix_case[pos_actuel[tour_joueur]]
        argent[2]+=prix_case[pos_actuel[tour_joueur]]
        Joueur_suivant()
    elif pos_actuel[tour_joueur] in possede[3]:
        affiche_texte_console2("vous devez payer le prix de la case a son propriéter")
        argent[tour_joueur]-=prix_case[pos_actuel[tour_joueur]]
        argent[3]+=prix_case[pos_actuel[tour_joueur]]
        Joueur_suivant()
    else:
        affiche_texte_console2("voulez vous acheter cette case? ")
        bouton_oui.place(x=692, y=170)
        bouton_non.place(x=800, y=170)

def acheter_case_actu():
    global pos_actuel, argent, prix_case, possede
    case_actuelle = pos_actuel[tour_joueur]
    montant_achat = int(prix_case[case_actuelle])
    argent[tour_joueur] -= montant_achat
    possede[tour_joueur].append(case_actuelle)
    


def bouton_oui():
    bouton_oui.place(x=-1000, y=-1000)
    bouton_non.place(x=-1000, y=-1000)
    acheter_case_actu()# Appeler la fonction pour acheter la case
    Joueur_suivant()
    
def bouton_non():
    print("Bouton 'non' a été cliqué.")
    bouton_oui.place(x=-1000, y=-1000)
    bouton_non.place(x=-1000, y=-1000)
    Joueur_suivant()
    

def changement_position_train():
    global pos_actuel, tour_joueur
    if pos_actuel[tour_joueur]==5:
        pos_actuel[tour_joueur]==15
    elif pos_actuel[tour_joueur]==15:
        pos_actuel[tour_joueur]==25
    elif pos_actuel[tour_joueur]==25:
        pos_actuel[tour_joueur]==35
    elif pos_actuel[tour_joueur]==35:
        pos_actuel[tour_joueur]==5
    
    Joueur_suivant()

def taxe_luxes():
    global tour_joueur, argent, bouton_suivant
    argent[tour_joueur]-=10000
    bouton_suivant.place(x=783, y=161)

def impot():
    global tour_joueur, argent, bouton_suivant
    argent[tour_joueur]-=20000
    bouton_suivant.place(x=783, y=161)
    
def compagni_deau():
    global tour_joueur, argent, bouton_suivant
    argent[tour_joueur]-=15000
    bouton_suivant.place(x=783, y=161)
    
def compagnie_elec():
    global tour_joueur, argent, bouton_suivant
    argent[tour_joueur]-=15000
    bouton_suivant.place(x=783, y=161)
    
def case_prison():
    global tour_joueur, bouton_suivant, pos_actuel, argent
    argent[tour_joueur]-=5000
    pos_actuel[tour_joueur]=10
    bouton_suivant.place(x=783, y=161)

def parc():
    global argent, tour_joueur, bouton_suivant
    argent[tour_joueur]+=nbparc()
    bouton_suivant.place(x=786, y=161)
    
    
    
def fonction_reconnaissance_case():
    global pos_actuel, tour_joueur 
    if pos_actuel[tour_joueur] in [1, 3, 6, 8, 9, 11, 13, 14, 16, 18, 19, 21, 23, 24, 26, 27, 29, 31, 32, 34, 37, 39]:
        acheter_case() 
        
    elif pos_actuel[tour_joueur] in [5, 15, 25,35]:
        affiche_texte_console("Vous êtes sur une case train.")
        affiche_texte_console2("Vous allez etre teleporter au prochain train ")
        changement_position_train()
        
        
        
    elif pos_actuel[tour_joueur] in [7, 22, 36]:
        affiche_texte_console("Vous êtes sur une case chance.")
        case_chance()
        
    elif pos_actuel[tour_joueur] in [2, 17, 33]:
        affiche_texte_console2("Vous êtes sur une case communautaire")
        bouton_suivant.place(x=783, y=161) 
        
        
    elif pos_actuel[tour_joueur] in [4]:
        affiche_texte_console2("Vous êtes sur la case impos sur le revenue .")
        impot()

    elif pos_actuel[tour_joueur] in [38]:
        affiche_texte_console("Vous êtes sur la case taxe luxes")
        taxe_luxes()
        
    elif pos_actuel[tour_joueur] in [28]:
        affiche_texte_console("Vous êtes sur la case compagnie d'eau ")
        compagni_deau()
        
    elif pos_actuel[tour_joueur] in [12]:
        affiche_texte_console("Vous êtes sur la case compagnie d'electrécité")
        compagnie_elec()
        
    elif pos_actuel[tour_joueur] in [10]:
        affiche_texte_console("Vous êtes sur la case prison")
        case_prison()
        
    elif pos_actuel[tour_joueur] in [30]:
        affiche_texte_console("Vous êtes sur la case bus en direction de la prison ")
        affiche_texte_console2("vous aller en prison et passer et perdez 5000f")
        case_prison()
        
    elif pos_actuel[tour_joueur] in [20]:
        affiche_texte_console("Vous êtes sur la case parc")
        affiche_texte_console("Vous faite une pause et trouver de l'argent au sol")
        parc()

########## Interface graphique ##########################
##########################################################
Mafenetre = Tk()
Mafenetre.title("Titre")
Canevas = Canvas(Mafenetre,width=1000,height=1000,bg ='white')
Canevas.pack()
font = Font(family='Liberation Serif', size=500) # création d'une police pour l'affichage du texte



Mapolice = Font(family='Liberation Serif', size=500) # création d'une police pour l'affichage du texte
Canevas.create_text(475,10,text="joueur 1",fill="black",font=Font)
Mapolice = Font(family='Liberation Serif', size=200) # création d'une police pour l'affichage du texte
Canevas.create_text(100,425,text="joueur 2",fill="black",font=Font)
Mapolice = Font(family='Liberation Serif', size=200) # création d'une police pour l'affichage du texte
Canevas.create_text(875,425,text="joueur 3",fill="black",font=Font)
Mapolice = Font(family='Liberation Serif', size=200) # création d'une police pour l'affichage du texte
Canevas.create_text(475,750,text="joueur 4",fill="black",font=Font)

Lab_argent_joueur1 = Label(Mafenetre, text=argent[0], fg='black', bg='white')
Lab_argent_joueur1.place(x=439, y=28)
Lab_argent_joueur2 = Label(Mafenetre, text=argent[1], fg='black', bg='white')
Lab_argent_joueur2.place(x=64, y=441)
Lab_argent_joueur3 = Label(Mafenetre, text=argent[2], fg='black', bg='white')
Lab_argent_joueur3.place(x=838 , y=444)
Lab_argent_joueur4 = Label(Mafenetre, text=argent[3], fg='black', bg='white')
Lab_argent_joueur4.place(x=442, y=767)

bouton_de=Button(Mafenetre, text="lancer le dé", command=Dé)
bouton_de.place(x=25,y=50)

Lab2= Label(Mafenetre, text=[], fg='black', bg='white')
Lab2.place(x=562, y=38)

fichier=PhotoImage(file='monopoly2.gif')
img=Canevas.create_image(500,400,image=fichier)



image=Canevas.create_rectangle(554,5,989,151,fill='white')#rectaglueu console

bouton_reco=Button(Mafenetre, text="recommencer", command=recommencer) # bouton recommencer
bouton_oui = Button(Mafenetre, text="    Oui    ", command=bouton_oui) #bouton oui
bouton_non = Button(Mafenetre, text="    Non    ", command=bouton_non) #bouton non
bouton_commencer = Button(Mafenetre, text="commencer", command=bouton_commencer) #bouton commencer
bouton_suivant = Button(Mafenetre, text="          j'ai lue, suivant          ", command=Joueur_suivant)
                        
bouton_suivant.place(x=-783, y=-161)                
bouton_oui.place(x=800,y=161)
bouton_non.place(x=900,y=161)
bouton_commencer.place(x=24, y=24)
bouton_reco.place(x=-1000,y=-1000)


point_joueur_0 = Canevas.create_oval(426, 7, 436, 17, fill="yellow")
point_joueur_3 = Canevas.create_oval(425, 746, 435, 756, fill="green")
point_joueur_2 = Canevas.create_oval(825, 422, 835, 432, fill="blue")
point_joueur_1 = Canevas.create_oval(50, 420, 60, 430, fill="red")


piont_joueur_0 = Canevas.create_oval(426, 7, 436, 17, fill="yellow")
piont_joueur_3 = Canevas.create_oval(425, 746, 435, 756, fill="green")
piont_joueur_2 = Canevas.create_oval(825, 422, 835, 432, fill="blue")
piont_joueur_1 = Canevas.create_oval(50, 420, 60, 430, fill="red")

joueurs.append(piont_joueur_0)
joueurs.append(piont_joueur_1)
joueurs.append(piont_joueur_2)
joueurs.append(piont_joueur_3)

###########################################################
########### Receptionnaire d'évènement ####################
###########################################################

def clic(event):
    x=event.x
    y=event.y
    print(x,",",y)

Canevas.bind('<Button-1>',clic)

##########################################################
############# Programme principal ########################
##########################################################
Lab = Label(Mafenetre, text="", fg='black', bg='white')
Lab.place(x=562, y=12)
bouton_oui.place(x=-1000, y=-1000)
bouton_non.place(x=-1000, y=-1000)
bouton_de.place(x=-1000, y=-1000)
affiche_texte_console("bonjours pour commencer le jeux veuiller clicker sur le bouton commencer")


###################### FIN ###############################  
Mafenetre.mainloop()

'''  
# Tests
print(lancer_de())
print(lancer_de_carte_chance())
print(deplacer_pion(5, 3))
case_chance(1)  


'''
